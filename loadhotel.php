<?php 
    session_start();
    require 'includes/dbconn.php';
    $limit = $_POST['hotelCountnew'];
    $hotels = $pdo->prepare("SELECT  * FROM hotels ORDER BY id DESC limit  $limit");
    $hotels->execute();
	$query ="SELECT * FROM countries";
	$results = $pdo->query($query);
?>
<div class="container">
    <?php foreach($hotels as $hotel): ?>
    <?php if($hotel['offered_price'] > '0'): ?>
    <?php

$idHot = $hotel['country_id'];
$query = $pdo->prepare("SELECT name FROM `countries` WHERE `id` = :idhot");   
$query->execute(['idhot' => $idHot]);
$nameCountry = $query->fetch();
$nameCountry = array_unique($nameCountry);
$string_versionCountry = implode('.',$nameCountry);

$idCity = $hotel['city_id'];
$query = $pdo->prepare("SELECT name FROM `cities` WHERE `id` = :idcity");   
$query->execute(['idcity' => $idCity]);
$nameCityArr = $query->fetch();
$nameCityArr = array_unique($nameCityArr);
$string_versionCity = implode('.', $nameCityArr)
?>
    <div id="offers">
        <div class="img-offers">
            <img src="images/<?php echo $hotel['image']; ?>" alt="offer image">
        </div>
        <div class="offers-content">
            <div class="header-offers">
                <p class="offer-price">Only <?php echo $hotel['offered_price'];?>€</p>
                <p class="offer-date"><?php echo $hotel['updated_at']; ?></p>
            </div>
            <div class="titulli-offertes">
                <h3><?php echo $hotel['name']; ?></h3>
            </div>
            <div class="info-offertes">
                <p><a href="#"><?php echo $string_versionCity; ?>-<?php echo $string_versionCountry; ?></a></p>
                <p><a href="#"></a><?php echo $hotel['company_name']; ?></p>
                <p><del><?php echo $hotel['current_price']; ?>Euro</del></p>
                <p><a href="./comments.php?id=<?php echo $hotel['id']; ?>">Comments</a></p>
            </div>
            <div class="teksti-offertes">
                <p><?php echo $hotel['description']; ?></p>
            </div>
            <?php if(isset($_SESSION['name'])) :?>
            <div class="button-book">
                <a href="./bookoffers.php?id=<?php echo $hotel['id']; ?>" class="button-book-btn">BOOK NOW</a>
            </div>
            <?php endif;?>
            <?php if(!isset($_SESSION['name'])) :?>
            <div class="offer-login">
            <p class="offer-login">Login or</p>
            <a href="signup.php">Signup to Book it</a>
            </div>
            <?php endif;?>
        </div>
    </div>
<?php endif;?>
<?php endforeach;?>
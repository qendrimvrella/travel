<?php include 'header.php'; ?>
<?php
	require_once("includes/dbconn.php");
	$query ="SELECT * FROM countries";
	$results = $pdo->query($query);
?>

    <section>
        <div class="container">
            <div class="showcase">
                <p></p>
                <div class="showcase-content">
                    <h1>Travel More For Fun</h1>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Vel tenetur dicta itaque, est rerum sunt delectus, iusto illo provident temporibus mollitia, ex natus consectetur! Tempora?</p>
                </div>
                <form class="book-form" action="bookhotel.php" method="GET" name="book" onsubmit="return validateBook()">
                    <div class="input-con">
                        <label for="country">Country</label>
                        <select  name="country" id="country">
                                <option value disabled selected >Select Country</option>
                                <?php foreach($results as $country): ?> 
                                    <option value="<?php echo $country["id"];?>"><?php echo $country["name"]; ?></option>
                                <?php endforeach;?>
                        </select>
                    </div>
                    <div class="input-con">
                        <label for="checkin">Check In</label>
                        <input type="date" name="checkin" id="checkin">
                    </div>    
                    <div class="input-con">
                        <label for="checkout">Check Out</label>
                        <input type="date" name="checkout" id="checkout">
                    </div> 
                    <div class="input-con">
                        <label for="checkout">Rooms</label>
                        <select name="rooms" id="rooms">
                                <option value="1">1</option>
                                <option value="2">2</option>
                        </select>
                    </div>
                    <div class="input-con">   
                        <button type="submit" name="submit" value="submit" class="btn">Search</button>
                    </div> 
                </form>
            </div>
        </div>
    </section>

    <div id="facilities">
        <div class="container">
            <div class="facilities-box">
                <img src="img/icon_1.svg" alt="Hotel">
                <h1>Beautiful Rooms</h1>
                <p>In vitae nisi aliquam, scelerisque leo a, volutpat sem. Vivamus rutrum dui fermentum eros hendrerit, id lobortis leo volutpat.</p>
            </div>
            <div class="facilities-box">
                <img src="img/icon_2.svg" alt="Pool">
                <h1>Swimming Pool</h1>
                <p>In vitae nisi aliquam, scelerisque leo a, volutpat sem. Vivamus rutrum dui fermentum eros hendrerit, id lobortis leo volutpat.</p>
            </div>
            <div class="facilities-box">
                <img src="img/icon_3.svg" alt="Resort">
                <h1>Luxury Resort</h1>
                <p>In vitae nisi aliquam, scelerisque leo a, volutpat sem. Vivamus rutrum dui fermentum eros hendrerit, id lobortis leo volutpat.</p>
            </div>
        </div>
    </div>

    <div id="quote">
        <div class="container">
            <img src="img/quote-img.jpg" alt="img">
            <div class="quotes">
                <p>“The gladdest moment in human life is a departure into unknown lands.” – <span class="font-color-black">Sir Richard Burton</span></p>
                <p>“Traveling – it leaves you speechless, then turns you into a storyteller.” – <span class="font-color-black">Ibn Battuta</span></p>
                <p>“The most beautiful thing in the world is, of course, the world itself.” - <span class="font-color-black">Wallace Stevens</span></p>
            </div>
        </div>
    </div>

<?php include 'footer.php'; ?>
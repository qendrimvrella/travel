<?php include 'header.php'; ?>
<?php include 'includes/signup.inc.php'; ?>
<div id="signup-con">
        <h1>Sign Up</h1>
        <form action="<?php $_SERVER['PHP_SELF'];?>" method="post">
            <div class="input-con">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" value="<?php echo $name?>">
                <span class="error-font"><?php echo $name_error?></span>
            </div>
            <div class="input-con">
                <label for="email">Email</label>
                <input type="email" name="email" id="email" value='<?php echo $email?>'>
                <span class="error-font"><?php echo $email_error?></span>
            </div>    
            <div class="input-con">
                <label for="password">Password</label>
                <input type="password" name="password" id="password">
                <span class="error-font"><?php echo $password_error?></span>
            </div>    
            <div class="input-con">
                <label for="password-confirm">Confirm Password</label>
                <input type="password" name="confirmpassword" id="password-confirm">
                <span class="error-font"><?php echo $confirmpassword_error?></span>
            </div>
            <div class="input-con">
                <label for="password-confirm">Are you a company?</label>
                <input type="checkbox" name="iscompany" id="is-company">
            </div>
            <button type="submit" class="btn">Sign Up</button>
            <br>
            <span class="success-font"><?php echo $success?></span>
        </form>
        <p>By clicking the Sign Up button, you agree to our <a href="#">Terms & Conditions</a> and <a href="#">Privacy Policy</a></p>
</div>


<?php include 'footer.php'; ?>
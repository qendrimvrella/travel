<?php
    $name_error = $email_error = $password_error = $confirmpassword_error = "";
    $name = $email = $password = $confirmpassword = '';
    $iscompany = (isset($_POST['iscompany']))? '1':'0';
    $success='';
    
    //eshte nje funksijon qe inputet i trajton si stringje
   

    if($_SERVER["REQUEST_METHOD"] == "POST"){
       
            require 'includes/dbconn.php';
            //kit funksion e kam kriju per me check nese gjindet imella n databas
            //pranon ne parametrin e par imellen qe duhet me u shiqu nese eksiston n databas
            //n parametrin e dyt databasen($pdo)
            function exists($email, $database){
                $value=false;
                $query = $database->prepare('SELECT * FROM users WHERE email=:email');
                $query->execute([':email'=>$email]);
                if($query->fetch()!=false){
                    $value=true;
                }
                return $value;        
            }
            

            //shiqojm neser eshte e zbrazet apo nese ka karaktere tjera perveq shkronjave dhe hapsirave
            if(empty($_POST["name"])){
                $name_error = "name is required!";
            }else{
                $name =ucfirst(test_input($_POST["name"]));
                if(!preg_match("/^[a-zA-Z ]*$/",$name)){
                    $name_error = "Only letters and whitespaces are allowed!";
                }
            }
            //shiqojm nese eshte i zbrazet inputi i email-it 
            if(empty($_POST["email"])){
                    $email_error = "E-mail is required!";
            }else{
                
                $email = test_input($_POST["email"]);
                if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                    $email_error = "invalid e-mail format!";
                }
                //check nese eksiston n databas ky email(duke perdorur funksionin e krijuar ma lart)
                if(exists($_POST['email'],$pdo)){
                    $email_error="Email is alredy taken!";
                }                
            }

            if(empty($_POST["password"])){
                $password_error = "Password is required!";
            }//chechk nese passwordi eshte me i vogel se 8 karaktere
            elseif(strlen($_POST['password']) < 8){
                $password_error = "Password must be at least 8 characters!";  
            }

            //chechk nese confirmpassword eshte i zbrazet
            if(empty($_POST["confirmpassword"])){
                $confirmpassword_error = "Type the above password to confirm.";
            }//chechk nese password eshte njesoj me confirmpassword
            elseif($_POST['password'] != $_POST['confirmpassword']){
                $confirmpassword_error = "Password does not match!";
            }else{
                //mbush variablen password me passwordin e dhen ne input dhe beje hash
                $password = password_hash(test_input($_POST["password"]),PASSWORD_DEFAULT);
            }
            
            
        
        
        //nese nuk ka pasur asnje error procedo ne insertimin e te dhenave ne databas
        if($name_error == "" and $email_error == "" and $password_error == "" and $confirmpassword_error == ''){                
            $insert = $pdo->prepare('INSERT INTO users(name, email, password, is_company,  created_at) VALUES (?,?,?,?,?)');
            $insert->execute([$name, $email, $password, $iscompany, date("Y-m-d")." ".date("h:i:sa")]);
            $success = "You have been successfuly registred!";
            //zbraz variablat qe sna nevojiten
            $name = $email = $password = $confirmpassword = $iscompany = '';
        }        
    }

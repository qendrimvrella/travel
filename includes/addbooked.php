<?php

        session_start();

        require_once 'dbconn.php';

        if (isset($_GET['id'])) {

            $hotelId = $_GET['id'];
            
            $checkin = $_SESSION['checkin'];
            $checkout = $_SESSION['checkout'];
            $rooms = $_SESSION['rooms'];
            $userId = $_SESSION['id'];


            $query = $pdo->prepare("INSERT INTO `books` (`hotel_id`, `user_id`, `from`, `to`, `room`) VALUES (:hotelId, :userId, :checkin, :checkout, :room)");
            $query->execute([
                'hotelId' => $hotelId,
                'userId' => $userId,
                'checkin' => $checkin,
                'checkout' => $checkout,
                'room' => $rooms
            ]);

            $hotel = $pdo->prepare("SELECT * FROM `hotels` WHERE `id` = :id");
            $hotel->execute(['id' => $hotelId]);
            $hotelResult = $hotel->fetch();

            $hotelRooms = $hotelResult['rooms'];
            
            $updatedRomms = $hotelRooms - $rooms;
            $update = $pdo->prepare("UPDATE `hotels` SET `rooms` = :rooms WHERE `id` = :id");
            $update->execute(['rooms' => $updatedRomms, 'id' => $hotelId]);

            header("Location: ../mybooks.php?book=success");
        }    

    
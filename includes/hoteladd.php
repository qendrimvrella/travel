<?php 
    //inicializojm variablat per error
    $name_error = $description_error = $file_error = $current_price_error = $contry_error = $city_error  = $rooms_error= "";
    //inicializojm variablat qe do te marrim nga forma e pastaj i plotsojm brenda if-ave 
    $name = $description = $file = $current_price = $contry = $city =  $rooms = "";

    //marrim emrin e kompanis qe e shton hotelin permes sesionit per arsye qe me pas mapak fusha me plotsu klienti
    //extension e fotove qe lehohet mu bo upload ne sistem
    $allowed_image_extension = array(
        "png",
        "jpg",
        "jpeg"
    );
 
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        //marrim excextion e fotos
        $file_extension = pathinfo($_FILES["image"]['name'], PATHINFO_EXTENSION);
        require 'dbconn.php';


        $company_name = $_SESSION['name'];
        if(empty($_POST['name'])){

            $name_error = 'Empty Name Field';

        }else{

            $name = $_POST['name'];
        }
        if(empty($_POST['description'])){

            $description_error = "Description field is Required";

        }elseif(!preg_match("/^[a-zA-Z ]*$/",$_POST['description'])){

            $description_error = "Only letters and whitespaces are allowed!";
            
        }else{

            $description = $_POST['description'];

        }
        if(empty($_FILES["image"]['name'])){

            $file_error = 'Upload a photo';

        }elseif(!in_array($file_extension, $allowed_image_extension)){

            $file_error = 'Only /png/jpg/jepg';

        }else{

            $file = $_FILES["image"]['name'];
            $target = "./images/".basename($file);
            move_uploaded_file($_FILES['image']['tmp_name'], $target);

        }
        if(!preg_match("/^[0-9]*$/",$_POST['current_price'])){

            $current_price_error = "Only Numbers";

        }elseif(empty($_POST['current_price'])){

            $current_price_error = 'Price is needed';

        }else{

            $current_price = $_POST['current_price'];

        }
        if(!preg_match("/^[0-9]*$/",$_POST['rooms'])){

            $rooms_error = "Only Numbers";

        }elseif(empty($_POST['rooms'])){

            $rooms_error = 'Price is needed';

        }else{

            $rooms = $_POST['rooms'];

        }
        if(!preg_match("/^[0-9]*$/",$_POST['rooms'])){

            $rooms_error = "Only Numbers";

        }elseif(empty($_POST['rooms'])){

            $rooms_error = 'Type available rooms ';

        }else{

            $rooms = $_POST['rooms'];

        }
            if(empty($_POST['country'])){

            $contry_error = 'Chose a Country';

        }else{

            $country = $_POST['country'];
        }
        if(empty($_POST['city'])){

            $city_error = 'Chose a City';

        }else{

            $city = $_POST['city'];
        }
        if($name_error =="" and $description_error == "" and $file_error == "" and $current_price_error =="" and $contry_error =="" and $city_error ==""){

          
                $insert = $pdo->prepare("INSERT INTO `hotels` (`id`, `name`, `description`, `image`, `current_price`, `offered_price`, `rooms`, `company_name`, `country_id`, `city_id`, `updated_at`) VALUES (NULL, :name, :description, :image, :current_price, '', :rooms, :company_name, :country_id, :city_id, '')");

            $insert->bindParam(':name',$name);
            $insert->bindParam(':description',$description);
            $insert->bindParam(':image',$file);
            $insert->bindParam(':current_price',$current_price);
            $insert->bindParam(':rooms',$rooms);
            $insert->bindParam(':company_name',$company_name);
            $insert->bindParam(':country_id',$country);
            $insert->bindParam(':city_id',$city);
            
            $insert->execute();
            //japim mesazhin qe u krye me sukses
            $success = "You have been successfuly registred!";
            
                //zbazim variablat
            $name = $description = $file = $current_price = $contry = $city = $rooms = "";
        }
    }

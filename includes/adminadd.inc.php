<?php
    $name_error = $email_error = "";
    $name = $email = '';
    $success = '';
    
    //eshte nje funksijon qe inputet i trajton si stringje
   

    if($_SERVER["REQUEST_METHOD"] == "POST"){
       
            require 'includes/dbconn.php';
            //kit funksion e kam kriju per me check nese gjindet imella n databas
            //pranon ne parametrin e par imellen qe duhet me u shiqu nese eksiston n databas
            //n parametrin e dyt databasen($pdo)
            function exists($email, $database){
                $value=false;
                $query = $database->prepare('SELECT * FROM users WHERE email=:email');
                $query->execute([':email'=>$email]);
                if($query->fetch()!=false){
                    $value=true;
                }
                return $value;        
            }
            

            //shiqojm neser eshte e zbrazet apo nese ka karaktere tjera perveq shkronjave dhe hapsirave
            if(empty($_POST["name"])){
                $name_error = "name is required!";
            }else{
                $name = $_POST["name"];
                if(!preg_match("/^[a-zA-Z ]*$/",$name)){
                    $name_error = "Only letters and whitespaces are allowed!";
                }
            }
            //shiqojm nese eshte i zbrazet inputi i email-it 
            if(empty($_POST["email"])){
                    $email_error = "E-mail is required!";
            }else{
                
                $email = $_POST["email"];
                if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                    $email_error = "invalid e-mail format!";
                }
                //check nese eksiston n databas ky email(duke perdorur funksionin e krijuar ma lart)
                if(!exists($_POST['email'],$pdo)){
                    $email_error="Email does not exist!";
                }                
            }            
            
        
        
        //nese nuk ka pasur asnje error procedo ne insertimin e te dhenave ne databas
        if($name_error == "" and $email_error == ""){                
            $remove = $pdo->prepare('UPDATE users SET is_admin =1 WHERE name=? and email=?;');
            $remove->execute([$name, $email]);
            $success = "You made ".$name.":".$email." admin";
            //zbraz variablat qe sna nevojiten
            $name = $email ='';
        }        
    }

<?php 

    require_once 'dbconn.php';

    if (isset($_GET['id'])) {

        $bookId = $_GET['id'];
        $hotelId = $_GET['hotelid'];

        $query2 = $pdo->prepare("SELECT `room` FROM `books` WHERE `id` = :id");
        $query2->execute(['id' => $bookId]);
        $books = $query2->fetch();

        $bookedRoom = $books['room'];

        $hotel = $pdo->prepare("SELECT `rooms` FROM `hotels` WHERE `id` = :id");
        $hotel->execute(['id' => $hotelId]);
        $hotelResult = $hotel->fetch();

        $hotelRooms = $hotelResult['rooms'];

        $rooms = $hotelRooms + $bookedRoom;

        $update = $pdo->prepare("UPDATE `hotels` SET `rooms` = :rooms WHERE `id` = :id");
        $update->execute(['rooms' => $rooms, 'id' => $hotelId]);


        $query = $pdo->prepare("DELETE FROM `books` WHERE `id` = :bookId");
        $query->execute(['bookId' => $bookId]);

        header("Location: ../mybooks.php?delete=success");
    }
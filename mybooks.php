<?php
    include 'header.php';
    if(isset($_SESSION['iscompany']) && isset($_SESSION['isadmin'])){
        if($_SESSION['iscompany'] == '1' || $_SESSION['isadmin'] == '1'){
            die('Companies and admins are not allowed to book hotels!!');
        }
    }else{
        die('You are not loged in!');      
    }
?>
<?php 
    require_once 'includes/dbconn.php';
    
    $userId = $_SESSION['id'];

    $query = $pdo->prepare("SELECT * FROM `books` WHERE `user_id` = :userId ORDER BY `id` DESC");
    $query->execute(['userId' => $userId]);
    $books = $query->fetchAll(PDO::FETCH_ASSOC);

    $count = $query->rowCount();
?>
    
    <div id="mybooks">
        <div class="container">
            <h1>My Books</h1>
            <div class="book-con">
                <table>
                    <tr>
                        <th>Hoteli</th>
                        <th>CheckIn</th>
                        <th>CheckOut</th>
                        <th>Rooms</th>
                        <th>Days</th>
                        <th>Price</th>
                        <th>Remove</th>
                    </tr>
                    <?php foreach ($books as $book): ?>   
                    <?php
                        $hotleID = $book['hotel_id'];
                        $query = $pdo->prepare("SELECT * FROM `hotels` WHERE `id` = :hotel_id");
                        $query->execute(['hotel_id' => $hotleID]);
                        $hotels = $query->fetch();

                        $to = substr($book['to'], 8, 2);
                        $mounth = substr($book['to'], 5, 2) - substr($book['from'], 5, 2);

                        if (substr($book['to'], 5, 2) > substr($book['from'], 5, 2)) {
                            for ($i = 0; $i < $mounth; $i++) {
                                $to += 30;
                            }
                            $days = $to - substr($book['from'], 8, 2);
                        }else{
                            $days = $to - substr($book['from'], 8, 2);
                        }

                        $price = $hotels['current_price'] * $days * $book['room'];

                        if ($hotels['offered_price'] > 0) {
                            $price = $hotels['offered_price'] * $days * $book['room'];
                        }

                    ?> 
                    <tr>
                        <td><?php echo $hotels['name']; ?></td>
                        <td><?php echo substr($book['from'], 0, 10); ?></td>
                        <td><?php echo substr($book['to'], 0, 10); ?></td>
                        <td><?php echo $book['room']; ?></td>
                        <td><?php echo $days; ?></td>
                        <td><?php echo $price; ?></td>
                        <td><a href="includes/deletebook.php?id=<?php echo $book['id']. '&hotelid=' . $hotels['id']; ?>">Delete</a></td>
                    </tr>
                    <?php endforeach;?>
                </table>
            </div>  
            <?php
                if ($count > 0) {
                    echo "<form action='./generatepdf.php' method='POST'>
                            <input type='submit' value='Generate PDF' class='btn'/>
                        </form>";
                }
                else {
                    echo "<p></p>";
                }

                if (isset($_GET['delete']) && $_GET['delete'] == 'success') {
                    echo "<p class='error-font'>Book Deleted</p>";
                }
                elseif (isset($_GET['book']) && $_GET['book'] == 'success') {
                    echo "<p class='success-font'>Book Added</p>";
                }
                else {
                    echo "<p></p>";
                }
            ?> 
        </div>
    </div>

<?php include 'footer.php'; ?>